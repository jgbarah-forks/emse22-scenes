# Software development metrics: to VR or not to VR? - Replication Package

## Table of Contents

A high level of this reproduction package doc.

- [Scenes of the experiment](#scenes-of-the-experiment)
- [Participants questionnaire](#participants-questionnaire)
- [Results data](#results-data)
- [Results analysis](#results-analysis)
- [Participant video examples](#video-examples)

## Scenes

[Go here to see the scenes](https://thesis-dlumbrer.gitlab.io/emse22-scenes)

### Deploy them locally

The easiest way to do it is to go to the `scenes` folder and deploy the `scenes/index.html` file within a simple HTTP server (i.e. node http-server or python simple http server).

(Optional) Examples of http servers deploy:
```shell
# Using node [1]
$> npm install -g http-server
$> http-server

# Using python 2 [2]
$> python -m SimpleHTTPServer

# Using python 3 [3]
$> python -m http.server
```

> NOTE: You need an Elasticsearch with data, if you want to see the setup, please go to [Participant video examples](#video-examples) section

## Participants questionnaire

Inside the `questionnaire` folder there are in different file format, the questionnaire presented to the participants, the On-Screen tasks are in PDF format and the VR tasks are in the scene included:

- [Demographics](./questionnaire/demographics.md)
- [CHAOSS On-Screen tasks](./questionnaire/CHAOSS-on-screen.pdf)
- [Openshift On-Screen tasks](./questionnaire/Openshift-on-screen.pdf)
- [CHAOSS VR tasks](./scenes/dashboards/chaoss-dashboards.html)
- [Openshift VR tasks](./scenes/dashboards/openshift-dashboards.html)
- [Feedback](./questionnaire/feedback.pdf)


## Results data

In the `results` folder there are the CSV files of the answers (raw and formatted) of the participants:

- `results/raw.csv`: Contains the raw answers of the screen participants. [Click here to go to the file](./results/raw.csv)
- `results/demographics.csv`: Contains the formatted demographics answers of the screen participants. [Click here to go to the file](./results/demographics.csv)
- `results/correctness.csv`: Contains the formatted correctness answers of the screen participants. [Click here to go to the file](./results/correctness.csv)
- `results/difficulty.csv`: Contains the formatted task difficulty level answers of the screen participants. [Click here to go to the file](./results/difficulty.csv)
- `results/timing.csv`: Contains the formatted timing (completion time) answers of the screen participants. [Click here to go to the file](./results/timing.csv)
- `results/feedback.csv`: Contains the formatted feedback answers of the screen participants. [Click here to go to the file](./results/feedback.csv)

The formatted files are the same results but with the fields formatted for the comparison between VR and On-Screen participants.

> NOTE: The feedback answers are in Spanish. The results and analysis in the paper are translated into English.


### Results analysis

The analysis presented in the article, and the figures that are there were generated using a Jupyter notebook included in the `analysis` folder (`analysis.ipynb`), you can see the result of the analysis in the jupyter notebook and pdf.

For executing the notebooks you need install the `analysis/requirements.txt` file with `pip`.

#### Demographics

- [Python notebook](./analysis/demographics.ipynb): `./analysis/demographics.ipynb` 
- [PDF](./analysis/demographics.pdf): `./analysis/demographics.pdf`

#### Correctness

- [Python notebook](./analysis/correctness.ipynb): `./analysis/correctness.ipynb`
- [PDF](./analysis/correctness.pdf): `./analysis/correctness.pdf`

#### Difficulty level

- [Python notebook](./analysis/difficulty.ipynb): `./analysis/difficulty.ipynb`
- [PDF](./analysis/difficulty.pdf): `./analysis/difficulty.pdf`

#### Timing

- [Python notebook](./analysis/timing.ipynb): `./analysis/timing.ipynb`
- [PDF](./analysis/timing.pdf): `./analysis/timing.pdf`

#### Confounding variables

- [Python notebook](./analysis/otherconfounding.ipynb): `./analysis/otherconfounding.ipynb`
- [PDF](./analysis/otherconfounding.pdf): `./analysis/otherconfounding.pdf`

#### Feedback

- [Python notebook](./analysis/feedback.ipynb): `./analysis/feedback.ipynb`
- [PDF](./analysis/feedback.pdf): `./analysis/feedback.pdf`


All the figures and tables have been used in the article.

> NOTE: The feedback answers are in Spanish. The results and analysis in the paper are translated into English.



### Video examples

Here are a sample video of the setups of the participants solving part of the experiment in the two environments, the audio have been deleted due to privacy.

- [VR setup](./videos/participant_vr.mp4)
- [On-Screen setup](./videos/participant_screen.mp4)